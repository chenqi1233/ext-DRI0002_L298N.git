//% color="#B8860B" iconWidth=50 iconHeight=40
namespace L298N{
  
    //% block="L298N Motor Init Board[BOARD]E1Pin[E1]M1Pin[M1]E2Pin[E2]M2Pin[M2]  " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD" 
    //% E1.shadow="dropdown" E1.options="E1" 
    //% M1.shadow="dropdown" M1.options="M1"
    //% E2.shadow="dropdown" E2.options="E2"   
    //% M2.shadow="dropdown" M2.options="M2"
    export function L298NInit(parameter: any, block: any) {      
     let e1=parameter.E1.code;  
     let m1=parameter.M1.code;
     let e2=parameter.E2.code;  
     let m2=parameter.M2.code;
     let board=parameter.BOARD.code;
     
     Generator.addInclude("L298N","#include <L298N.h>");
     Generator.addObject(`L298N${board}` ,`L298N`,`tb${board}(${e1},${m1},${e2},${m2});`);
      
    }
    //% block="L298N Motor Board[BOARD]Motor[MOTOR]Dir[FR]Speed[SPEED]  " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD"    
    //% MOTOR.shadow="dropdownRound" MOTOR.options="MOTOR" 
    //% FR.shadow="dropdownRound" FR.options="FR"
    //% SPEED.shadow="range" SPEED.params.min="0" SPEED.params.max="255" SPEED.defl="200"
    export function L298NInit1(parameter: any, block: any) {        
     let board=parameter.BOARD.code;         
     let fr=parameter.FR.code;  
     let speed=parameter.SPEED.code;
     let m=parameter.MOTOR.code;

       Generator.addCode(`tb${board}.setSpeed(${m}, ${fr},${speed});`);
  
     }

    //% block="L298N Motor Board[BOARD]Motor[MOTOR]  " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD"    
    //% MOTOR.shadow="dropdownRound" MOTOR.options="MOTOR" 
    
    export function L298NInit2(parameter: any, block: any) {        
        let board=parameter.BOARD.code;         
        let m=parameter.MOTOR.code;
   
         Generator.addCode(`tb${board}.stop(${m});`);
 
           
        }
      
 
}