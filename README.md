# L298N直流电机驱动模块DF-MD V1.3
DF-MD V1.3大功率直流电机驱动器采用LGS公司优秀大功率电机专用驱动芯片L298N，可直接驱动2路直流电机，驱动电流达2A，电机输出端采用高速肖特基二极管作为保护。该电路线路布线合理、均采用贴元件片、体积小、方便安装。DF-MDV1.3增加了散热器，可以承受更大的电流；控制端口由原来的6个改为4个，不但可以节约控制器端口，而且控制程序也更为简单。使用DF-MD 模块可以较为轻松地驱动您的机器人。接线简便，可以直接与Arduino连接使用。

![](./arduinoC/_images/featured.png)

# 积木

![](./arduinoC/_images/blocks.png)

# 示例程序

**特别注意**

- UNO工作电压是5V，电机可以正常运行
- microbit、掌控用的两用扩展板电压是3.3V，使用时会出电机不转动，设置最高速度。或者外接电源。

- LOW = 0; HIGH = 1; PWM = 0~255
- PWM1,PWM2：分别为两个电机控制的使能端(可使用PWM调速)
- DIR1,DIR2：正反转控制信号输入端。比如，DIR1=0，M1电机正转; DIR1=1，M1电机反转。

 
![](./arduinoC/_images/example.png)

![](./arduinoC/_images/example1.png)



 

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|-----|-----|
|uno||√|||
|micro:bit||√|||
|mpython||√|||
|leonardo||√|||
|nano:bit||√|||
|mega2560||√|||

# 更新日志

V0.0.1 基础功能完成

